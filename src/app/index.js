// Core
import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';

// Other
import { client } from './init/client'; 

// Components
import { Pet } from './bus/pet';

export const App = () => {
  return (
    <ApolloProvider client={client}>
      <Pet />
    </ApolloProvider>
  );
}

